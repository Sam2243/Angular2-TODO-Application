import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
    public company = 'Dibuco';

    public entityName = '';

    public entity;

    public entityDescription = '';

    public entityList = [
      {'id': 1, 'name': 'Doc1.pdf', 'description': 'This is a dummy PDF file'},
      {'id': 2, 'name': 'Doc5.xlsx', 'description': 'This is a dummy Excel file'},
      {'id': 3, 'name': 'abc.json', 'description': 'This is a dummy JSON file'}
    ];

    public addEntity() {
      if(this.entity) {
         var id = this.entity.id;
         var result = this.entityList.filter(function(ent) {
           return ent.id == id;
         });
         result[0].name = this.entityName;
         result[0].description = this.entityDescription;
         this.entity = '';
         this.resetModel();
      } else {
        this.entityList.push({'id': this.entityList.length + 1, 'name': this.entityName, 'description': this.entityDescription});
        this.resetModel();
      }
    }

    public deleteEntity(entity) {
      const index = this.entityList.indexOf(entity);
      this.entityList.splice(index, 1);
    }

    public editEntity(entity) {
      this.entity = entity;
      this.entityName = entity.name;
      this.entityDescription = entity.description;
    }

    public resetModel() {
      this.entityName = '';
      this.entityDescription = '';
    }
}
