The project was generated through [Angular CLI](https://github.com/angular/angular-cli). There is only one pre-requistie to install Angular CLI which is to install [npm](https://www.npmjs.com/) Node Packaging Manager. Please follow the following steps to get this project up and running on your machine.

# Installation
if you are using linux the run the following command to install npm
- `sudo apt-get install npm`

if you are using windows then go the following link and run the setup
[NodeJS Downloads](https://nodejs.org/en/download/)

Check if npm is installed correctly with the following command on terminal or command prompt(Also make sure if you have set the environment variable on windows for it)
- `npm --version`

once npm is installed, install the Angular cli with the following command
- `npm install -g @angular/cli`
(Flag -g refers to install any utility globally so that you dont have do it on the next terminal session)

The next important thing to do is to install node modules which are required to run any angular project. In the project you will see `package.json` which consists of all the names and versions of node modules which will be installed. Go to WHERE_YOUR_PROJECT_IS then run the following command
- `npm install`
 
That's about it for setting up the environment.

# Running the project (Development server)
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
